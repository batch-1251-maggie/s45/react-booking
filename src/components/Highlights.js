import React from 'react';

//components
import {Row, Col, Card, Button} from 'react-bootstrap';


export default function Highlights(){
    return (

    <Row>
        <Col xs={12} md={4}>
            <Card>
                <Card.Body>
                    <Card.Title>Learn from Home</Card.Title>
                    <Card.Text>
                    Lorem lorem lorem lorem lorem lorem lorem lorem lorem.
                    </Card.Text>
                    <Button variant="primary">Go somewhere</Button>
                </Card.Body>
            </Card>`
        </Col>
        <Col xs={12} md={4}>
            <Card>
                <Card.Body>
                    <Card.Title>Study Now, Pay later</Card.Title>
                    <Card.Text>
                    Lorem lorem lorem lorem lorem lorem lorem lorem lorem.
                    </Card.Text>
                    <Button variant="primary">Go somewhere</Button>
                </Card.Body>
            </Card>`
        </Col>
        <Col xs={12} md={4}>
            <Card>
                <Card.Body>
                    <Card.Title>Be Part of Our Community</Card.Title>
                    <Card.Text>
                    Lorem lorem lorem lorem lorem lorem lorem lorem lorem.
                    </Card.Text>
                    <Button variant="primary">Go somewhere</Button>
                </Card.Body>
            </Card>`
        </Col>
    </Row>




    

    )
  }