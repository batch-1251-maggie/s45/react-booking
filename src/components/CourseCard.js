import React from 'react';

//components
import {Row, Col, Card, Button} from 'react-bootstrap';


export default function CourseCard(){
    return (

    <Row>
        <Col xs={12}>
            <Card>
                <Card.Body>
                    <Card.Title>Sample Course</Card.Title>
                        <h5 className="m-0">Description:</h5>
                        <p>This is a sample course offering.</p>
                        <h5 className="m-0">Price:</h5>
                        <p>PhP: 40,000</p>
                    <Button variant="primary">Enroll</Button>
                </Card.Body>
            </Card>`
        </Col>
    </Row>

    )
  }