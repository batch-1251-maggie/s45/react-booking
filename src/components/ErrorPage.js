import React from 'react';
import {Link} from 'react-router-dom';

//components
import {Container, Row, Col} from 'react-bootstrap';


export default function ErrorPage(){
    return (

        <Container fluid>
            <Row>
                <Col className="p-5 text-center">
                        <h1>404 - Not found</h1>
                        <p>The page you are looking for cannot be found</p>
                        <Link  to="/">Go back to Homepage</Link>
                </Col>
            </Row>
        </Container>
    )
  }