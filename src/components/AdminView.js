import React, {useEffect, useState} from 'react';

import {Container, Table, Button, Modal, Form} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AdminView(props){
    console.log(props)

    const {courseData, fetchData} = props
    console.log(courseData) //array of courses

    const [courseId, setCourseId] = useState('')
    const [courses, setCourses] = useState([])
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    // const [courseCreate, setCourseCreate] = useState('')

    const [showAdd, setShowAdd] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    

    let token = localStorage.getItem('token')


    //OPEN "ADD" COURSE MODAL FUNCTION
    const openAdd = () => {
    
        setShowAdd(true)
        setName("")
		setDescription("")
		setPrice(0)
    }

    //CLOSE "ADD" COURSE MODAL FUNCTION
    const closeAdd = () => {
        
        setShowAdd(false)
        setName("")
        setDescription("")
        setPrice(0)
    }


    //SUBMIT "ADD" COURSE MODAL WITH NEW COURSE DETAILS
    const addCourse = (e) => {
        e.preventDefault()

        fetch(`http://localhost:4000/api/courses/addCourse`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)

            fetchData()

            if(result === true){
                Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully created!'
                })
                closeAdd()

            } else {

                fetchData()

                Swal.fire({
					title: 'Failed',
					icon: 'error',
					text: 'Something went wrong!'
				})
            }
        })
    }


    //OPEN "UPDATE" MODAL
    const openEdit = (courseId) => {

        fetch(`http://localhost:4000/api/courses/${courseId}/edit`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)

            setCourseId(result._id)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
        })

        setShowEdit(true);
    }

    //CLOSE "UPDATE" MODAL FUNCTION
    const closeEdit = () => {

        setShowEdit(false)
        setName("")
        setDescription("")
        setPrice(0)

    }

    //SUBMIT "UPDATE" MODAL FUNCTION WITH NEW INPUT
    const editCourse = (e) => {
        e.preventDefault()

        fetch(`http://localhost:4000/api/courses/${courseId}/edit`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)

            fetchData()

            if(result.name, result.description !== '' && result.price !== null){
                Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully updated!'
                })
                closeEdit()

            } else {

                fetchData()

                Swal.fire({
					title: 'Failed',
					icon: 'error',
					text: 'Something went wrong!'
				})
            }
        })
    }

     //DISABLE BUTTON/ARCHIVE COURSE
     const archiveToggle = (courseId, isActive) => {
    
        fetch(`http://localhost:4000/api/courses/${courseId}/archive`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
			body: JSON.stringify({
				isActive: isActive
			})
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)

            if(result === true){

                fetchData()

                Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully archived!'
                })

            } else {

                fetchData()

                Swal.fire({
					title: 'Failed',
					icon: 'error',
					text: 'please try again'
				})
            }
        })

    }

    //ENABLE BUTTON/UNARCHIVE COURSE
    const unarchiveToggle = (courseId, isActive) => {
    
        fetch(`http://localhost:4000/api/courses/${courseId}/unarchive`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
			body: JSON.stringify({
				isActive: isActive
			})
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)

            if(result === true){

                fetchData()

                Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully unarchived!'
                })

            } else {

                fetchData()

                Swal.fire({
					title: 'Failed',
					icon: 'error',
					text: 'please try again'
				})
            }
        })
    }


    useEffect( () => {

        const coursesArr = courseData.map( (course) => {
            console.log(course)

            return(
				<tr key={course._id}>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
						{
							(course.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>
                        {/* UPDATE BUTTON */}
                        <Button variant="primary" size="sm"
                        onClick={ () => openEdit(course._id) }>
                            Update
                        </Button>
                        {/* IF COURSE IS ACTIVE SHOW DISABLE BUTTON
                            IF COURSE IS NOT ACTIVE SHOW ENABLE BUTTON */}
                        {
                            (course.isActive === true) ?
                                <Button variant="danger" size="sm" className="mx-1"
                                onClick={ () => archiveToggle(course._id,
                                course.isActive) }>
                                    Disable
                                </Button>
                            :
                                <Button variant="success" size="sm" className="mx-1"
                                onClick={ () => unarchiveToggle(course._id,
                                course.isActive) }>
                                    Enable
                                </Button>
                        }
					</td>
				</tr>
			)


        }) 

        setCourses(coursesArr)

    }, [courseData])


    return(
        <Container>
            <div>
                <h2 className="text-center">Admin Dashboard</h2>
                <div className="d-flex justify-content-end mb-2"> 
                    {/* ADD COURSE BUTTON */}
                    <Button variant="primary" 
                    onClick={() => openAdd()}>Add New Course</Button>
                </div>
            </div>
            <Table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {/*DISPLAY THE COURSES*/}
					{courses}
                </tbody>
            </Table>
            {/* EDIT COURSE MODAL */}
            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={ (e) => editCourse(e, courseId) }>
                    <Modal.Header>
                        <Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" value={name}
                                onChange={ (e) => setName(e.target.value )}
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" value={description}
                                onChange={ (e) => setDescription(e.target.value )}
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Price</Form.Label>
                                <Form.Control type="number" value={price}
                                onChange={ (e) => setPrice(e.target.value )}
                                ></Form.Control>
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        {/* MODAL BUTTONS */}
                        <Button variant="secondary" 
                        onClick={closeEdit}
                        >Close</Button>
                        <Button variant="success" type="submit"
                        >Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
            {/* ADD COURSE MODAL */}
            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={ (e) => addCourse(e) }>
                    <Modal.Header>
                        <Modal.Title>Add Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group courseId="courseName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" value={name}
                                onChange={(e) => setName(e.target.value)}
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group courseId="courseDesc">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" value={description}
                                onChange={(e) => setDescription(e.target.value)}
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group courseId="coursePrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control type="number" value={price}
                                onChange={(e) => setPrice(e.target.value)}
                                ></Form.Control>
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" 
                        onClick={closeAdd}
                        >Close</Button>
                        <Button variant="success" type="submit"
                        >Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Container>
        
    )
}