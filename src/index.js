import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

//bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

//components
import App from './App';


ReactDOM.render(
<Fragment>
  <App/>
</Fragment>,
document.getElementById('root')
);

