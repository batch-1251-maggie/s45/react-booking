export default [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus esse repellendus suscipit eaque blanditiis eius, officiis voluptas ratione quaerat sint. Odio architecto officiis qui, veniam atque ea suscipit sed nobis.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus esse repellendus suscipit eaque blanditiis eius, officiis voluptas ratione quaerat sint. Odio architecto officiis qui, veniam atque ea suscipit sed nobis.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus esse repellendus suscipit eaque blanditiis eius, officiis voluptas ratione quaerat sint. Odio architecto officiis qui, veniam atque ea suscipit sed nobis.",
        price: 55000,
        onOffer: true
    },
]