import React, {useState, useEffect} from 'react';

//context
import UserContext from './UserContext';

//router
import {BrowserRouter, Route, Switch} from "react-router-dom";


//components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import AppNavbar from './components/AppNavbar';
import ErrorPage from './components/ErrorPage';
import SpecificCourse from './pages/SpecificCourse';
// import Counter from './components/Counter';
// import CourseCard from './components/CourseCard';


export default function App(){

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });
    


    const unsetUser = () => {
        localStorage.clear();
        setUser({
            id: null,
            isAdmin: null
        })
    }

    useEffect( () => {
		let token = localStorage.getItem('token');
		fetch('http://localhost:4000/api/users/details', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result) //object/ document of a user

			if(typeof result._id !== "undefined"){
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	}, [])


    return (
        // <Fragment>
        //     <AppNavbar/>
        //     <Home/>
        //     <CourseCard/>
        //     <Courses/>
        //     <Counter/>
        //     <Register/>
        //     <Login/>
        // </Fragment>
    <UserContext.Provider value={{user, setUser, unsetUser}}>
        <BrowserRouter>
            <AppNavbar/>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/courses" component={Courses} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/courses/:courseId" component={SpecificCourse} />
                <Route exact path="" component={ErrorPage} />
            </Switch>
        </BrowserRouter>
    </UserContext.Provider>
    )
  }