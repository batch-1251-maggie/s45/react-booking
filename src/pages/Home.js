import React from 'react';

//components
import {Container} from 'react-bootstrap';

import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

export default function Home(){

    return (
            <Container className="p-4">
              <Banner/>
              <Highlights/>
            </Container>
    )
  }