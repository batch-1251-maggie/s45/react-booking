import React, {useState, useContext, useEffect} from 'react';

import {Container} from 'react-bootstrap';

import UserContext from './../UserContext';

//components
import Course from './../components/Course';
import courses from './../mock-data/courses';
import AdminView from './../components/AdminView';
import UserView from './../components/UserView';

export default function Courses(){

const [courses, setCourses] = useState([]);

const {user, setUser} = useContext(UserContext);


const fetchData = () => {
  let token = localStorage.getItem('token')

  fetch('http://localhost:4000/api/courses/all', 
    {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${token}`
      }
  })
  .then(result => result.json())
  .then(result => {
      // console.log(result)
      setCourses(result)
  })

}

useEffect( () => {
  fetchData()
}, [] )



    return (
            <Container className="p-4">
              { (user.isAdmin === true) ?
                  <AdminView courseData={courses} fetchData={fetchData}/>
                :
                  <UserView courseData={courses}/>
              }
            </Container>
    )

  }